## Giorgi Mskhiladze

* Esbrina la versió exacte del servei web.

* Esbrina la versió dels altres serveis

* Enumera tots els usuaris del sistema utilitzant els serveis i la informació que haguis pogut obtenir de la web. (Hi ha 5 usuaris creats més els que venen per defecte)

* Esbrina els passwords de tots els usuaris del sistema que en tinguin.

* Publica una notícia a la web

* Fes un informe de tot

# Esbrinar IP máquina

Primer de tot executaré un **nmap** en tota la xarxa per tal d'esbrinar la IP que té la máquina:

```
nmap 172.16.9.0/16
```

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/nmap1.png "nmap1")

Com ja sabem la IP que té la máquina, executaré un nmap més exhaustiu i averiguaré el servei que hi ha en cada port i la seva versió:

```
nmap -sV -T4 -p 1-10000 172.16.9.182
```

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/nmap2.png "nmap2")

Página Web
======

Com podem veure en el nmap hem trobat que en el port 81, hi ha un servidor web, si entrem dins veiem que hi ha una página web creada:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/pagina1.png "pagina1")

Si entrem dins del apartat de **Seguretat Informatica**, baixem i veiem que hi ha un apartat que diu **tauler**, on si fem click ens porta en un inici de sesió de Wordpress:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/pagina2.png "pagina2")

Esbrinar usuaris
======

Per tal d'esbrinar els usuaris que té el sistema, ens conectem mitjançant el telnet en el port 25, que és la de SMTP, i començem a probar amb nom d'usuaris que podrien existir, veiem que descobrim 3:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/smtp1.png "smtp1")

Ara editarem el fitxer **/usr/share/nmap/nselib/data/usernames.lst**, i dins només posarem el usuari de **kristinecastillo**.

Després obrim un nou terminal i executem la següent comanda:

```
nmap -p 22 --script ssh-brute --script-args userdb=users.lst,passdb=pass.lst --script-args ssh-brute.timeout=4s 172.16.9.182
```

I ens esperem a que es trobi la contrasenya:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/smtp2.png "smtp2")

Veiem que la contraseña del usuari **kristinecastillo**, es com el seu nom d'usuari.

SSH
=====

Ara ens conectarem amb ssh dins del servidor amb el usuari que acabem de descobrir:

```
ssh kristinecastillo@172.16.9.182
```

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/ssh1.png "ssh1")

Si ens anem en el **/home**, podrem veure els usuaris del sistema:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/ssh2.png "ssh2")

Ja tenim una contrasenya de 5!

**kristinecastillo:kristinecastillo**

Katrina Craig
======

Per descobrir la contrasenya de Katrina, només ens tindrem que llegir la seva descripció en la página de Wordpress:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/ssh3.png "ssh3")

Com podem veure li agrada molt la Wonder Woman.

Anem a probar aquesta contrasenya, i... PREMI!

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/ssh4.png "ssh4")

Ja tenim un altre contrasenya:

**katrinacraig:wonderwoman**

William
======

Per treure la contrasenya del usuari **william.flores**, tornarem a executar el script del nmap, tornant a modificar la llista d'usuaris i esperant un temps:

```
nmap -p 22 --script ssh-brute --script-args userdb=users.lst,passdb=pass.lst --script-args ssh-brute.timeout=4s 172.16.9.182
```

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/ssh5.png "ssh5")

Veurem que la contrasenya és **matrix**:

**william.flores:matrix**

Mail
======

Al entrar, veiem que té correu, anirem i llegirem el correu per tal de veure que hi ha escrit dins:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/ssh6.png "ssh6")

Veiem que al Johnny li agrada molt Messi, intentarem d'entrar amb aquesta contrasenya a veuere que pasa:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/MaquinaSalo/Imagenes/ssh7.png "ssh7")

I ja tindriem un altre contrasenya:

**johnnym:messi**

