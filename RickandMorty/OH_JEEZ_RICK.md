Giorgi Mskhiladze


Enunciat
======

**Has de trobar 130 punts en flags i fer una writeup**

Descubrir IP i ports
======

Per tal de descobrir la IP, enguegarem un KaliLinux, i executarem la següent comanda, per tal de descobrir la IP que té la máquina de Rick and Morty: 

```
nmap -sS 192.168.56.0/24 -T4
```

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/1.png "nmap")

Ara mirarem quins ports té oberta la máquina escanejant tots els pots que hi han:

```
nmap -sS 192.168.56.108 -p 1-65535 -T4 | tee nmap-portScan
```

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/2.png "nmap")

Ara averiguarem més informació encara dels ports, amb la següent comanda:

```
nmap -sS 192.168.56.108 -p 21,22,88,9090,13337,22222,60000 -T4 -sV -O | tee nmap-portScan-serviceVersion-and-05
```

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/3.png "nmap")

Ja tinc 10 punts gracies al port 13337! 

Página web
=======

Ara obrirem el port 9090 en un navegador web, ja que és un http:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/4.png "http")

Uns altres 10 punts!

Port 60000
=======

La següent cosa que farem será veure que tenim dins del port 60000, ja que no hi ha ningún servei que el nmap detecti, per tal agafarem el banner del port per veure que és:

```
nc 192.168.56.108 60000
```

Podrem veure que és una shell on hi ha un arxiu, al fer el **cat** del arxiu, podrem veure que obtenim uns altres 10 punts!

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/5.png "60000")

Port 80
======

Com ja hem vist el port 60000, probarem amb el 80, per veure que hi ha:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/6.png "80")

En aquesta página si ens mirem el codi font de la página, amb el **OSASP-ZAP**, trobarem el arxiu robots.txt:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/7.png "80")

Si entrem en el arxiu, ens trobem amb el següent:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/8.png "80")

Si anem entrant en cada página web que tenim en el arxiu, podrem veure que només la **/cgi-bin/tracertool.cgi**, té alguna cosa interesant:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/9.png "80")

Si posem una IP, podrem veure que fa un **traceroute** de la IP introduida.

Si fem un atac de injeció, veurem que ens deixa, el qual és una bona senyal:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/10.png "80")

Si posem la següent comanda, veurem que ens saltem el traceroute peró la comanda **cat** ha sigut substituit per un dibuix d'un gat, per tant tindrem que utilitzar el **tail**:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/11.png "80")

Fem un **tail** al fitxer **/etc/passwd**, i veurem que ens dona 3 usuaris:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/12.png "80")

Ja tenim 3 usuaris amb els que treballar: **RickSanchez, Morty, Summer**. Posarem els usuaris dins d'un arxiu que es dirá **users**.

Si ara tornem i executem un examen més exhaustiu amb la eina de **OSASP-ZAP**, trobem que existeix el directori **/passwords**. Dintre tenim dos fitxers:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/13.png "80")

En el primer fitxer, trobem 10 punts!

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/14.png "80")

En l'altre página, dins del codi font de la página, trobem la contrasenya **winter**:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/15.png "80")

La contrasenya la posarem en un arxiu que es dirá **passwd**

Hydra
=======

Amb la contrasenya que hem trobat, executarem el **Hydra** dins del kali, y farem un atac contra el FTP amb cada usuari. 

Finalment trobarem que la contrasenya és del usuari **Summer**:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/16.png "hydra")

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/17.png "hydra")

Ja sabem que amb el usuari Summer, podrem entrar per FTP. Ara ho probarem amb el SSH, i si ens funciona podrem entrar dins de la máquina virtual, **RECORDAR QUE EL PORT DE SSH ES 22222 I NO 22**:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/18.png "hydra")

Ens conectarem amb el ssh al compte de Summer, i veurem que en la seva carpeta té el fitxer FLAG.txt, que si el analitzem, veurem que ens dona uns altres 10 punts:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/19.png "hydra")

Si naveguem una mica, podrem veure que el usuari de Summer, pot entrar en els homes de Morty i de Rick:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/20.png "hydra")

Si ens mirem els arxius que hi han en les homes de Rick y de Morty, podrem trobar que si ens mirem la cabecera del fitxer **/home/Morty/Safe_Password.jpg** que hi ha una línea curiosa, que ens diu la contrasenya per el fitxer **journal.txt.zip** (Meeseek):

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/21.png "hydra")

Si utilitzem aquesta contrasenya per tal de descomprimir aquest arxiu, veurem que ens donen uns altres 20 punts:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/22.png "hydra")

A més si ens lleguim el texte, veurem que ens donen la contrasenya de la caixa de seguretat de Rick (131333).

Si ens anem en la caixa fort de Rick, i analitzem el fitxer, veurem que no ens deixa executar, ja que no tenim permisos per tal de fer-ho, peró el fitxer està creat de forma que els permisos no el tingui sempre el Rick, per tant si el copiem en la home de Summer, podrem executar el fitxer.

Com es una caixa forta, si intentem executar el fitxer amb la contrasenya que hem trobat abans, ens donará uns altres 20 punts!:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/23.png "hydra")

Rick
======

Si ens lleguim el que ens diu Rick, podrem veure que és una pista per la contrasenya del usuari **RickSanchez**, veiem que ens diu que la contrasenya és una paraula de la banda vella en la que estaba el Rick, si ens mirem per internet veurem que Rick estaba en una banda que es deia **The Flesh Curtains**, per tant ara tindrem que generar un arxiu de contrasenyes que tingui un número, una letra majuscula i cada paraula que conté el nom de la banda vella de Rick.

Per generar el fitxer, utilitzarem el **maskprocessor**, per tal de generar el fitxer:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/24.1.png "rick")

Ara si executem una atack amb el hydra contra el usuari RickSanchez amb les contrasenyes que hem generat, veurem que la contrasenya és **P7Curtains**:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/25.png "rick")

Ara si ens conectem amb el Rick mitjançant el ssh, i intentem accedir amb el usuari root, veurem que ens deixa:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/26.png "rick")

Com ja som root, podrem buscar la paraula **FLAG** dins de cada directori i podrem obtenir els 130 punts:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/27.png "rick")

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/RickandMorty/Imagenes/28.png "rick")

I ja tindriem les 130 punts!