### Giorgi Mskhiladze

Descobrir IP i serveis máquina Metasploiteable2
===========

Tinc la máquina posada amb xarxa només-amfitrió, per tal d'averiguar la IP que té la máquina i poder fer atacs, obrire una máquina Kali i executaré un nmap en la xarxa del amfitrió:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/nmap.png "nmap")

Aixó ho hem descobert amb la comanda: 

```
nmap -v 192.168.56.0/24
```

Ara per descobrir les versions de cada servei per tal de fer el atac, executarem la següent comanda:

```
nmap -sV 192.168.56.102
```

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/nmap2.png "nmap2")

Com veiem no tenim que tornar a analitzar tota la xarxa, ja que ara tenim la IP de la máquina.

Atac a VSFTPD
======

Començarem llançant el primer atac al VSFTPD de la máquina, per aixó, simplement tindrem que obrir el Metasploit, per aixó només tindrem que fer click aquí:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/metasploit.png "metasploit")

Un cop s'ha inciat el metasploit, posarem la següent comanda per tal de buscar les vulnerabilitats del VSFTPD:

```
search VSFTPD
```

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/metasploit2.png "metasploit2")

> Amb la comanda "search" podrem buscar vulnerabilitats dels serveis que posem després, és a dir, la comanda funciona d'aquesta forma:
> ```
> search SERVEI_A_CERCAR
> ```

Ara com volem explotar aquesta vulnerabilitat, el que tenim que fer, es utilitzar la comanda **use** i després posar el nom de la vulnerabilitat:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/metasploit3.png "metasploit3")

> Amb la comanda "use" podrem utilitzar la vulnerabilitat que haguem trobat amb la comanda "search"
> ```
> use VULNERABILITAT_A_EXPLOTAR
> ```

Ara per tal de sapiguer com fer el atac, tindrem que escriure la comanda **show options**, i veurem quines dades tenim que posar i quines ja tenim posades:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/metasploit4.png "metasploit4")

Com podem veure tenim posades quasi totes les opcions, només ens falta la opció de **RHOST**, per tal de posar també aquesta opció, només tenim que escriure:

```
set RHOST (VALOR)
```

> Amb el **set** podrem posar un valor a la variable que tenim
> ```
> set VARIABLE VALOR_QUE_POSAREM
> ```

Un cop ho tenim, escriurem **run** i executarem el exploit:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/metasploit5.png "metasploit5")

### CVE
**CVE-2011-0762:** 
https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-0762 

Telnet
======

Ara atacare el telnet, per aixó, només caldrá posar la comanda:

```
telnet IP_MAQUINA
```

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/telnet1.png "telnet1")

Com podem veure, tenim un banner bonic la qual ens diu el usuari i la contraseña per tal d'entrar. 
Aixó no té cap CVE, és una prova per tal de veure que hi havia en el telnet, ja que estava oberta.

Samba
======

Ara atacaré el Samba, per aixó tindrem que tornar en el metasploit i buscar les vulnerabilitats que en té el Samba:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/samba1.png "samba1")

Veiem que tenim moltes opcions per escollir, per reduir els atacs, executarem de nou un nmap més exhaustiu per tal de descobrir la versió i altres coses, per aixó executarem la següent comanda:

```
nmap -PA -A -sV -sT -T4 --version-all -v -p 445 IP_MAQUINA
```

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/samba2.png "samba2")

Com veiem el samba és la versió **3.0.20-Debian**, si ara busquem:

```
samba 3.0.20 cve
```

Veurem que les dos primeres busquedes, ja son un exploit del samba, entrarem en la página de **Rapid7**, la qual ens diu el exploit més comú, i la segona página podem veure el CVE en concret:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/samba3.png "samba3")

En Rapid7, trobarem el número de CVE: **CVE-2007-2447**

Si aquesta la busquem en la página oficial de CVE https://www.cvedetails.com/, veurem que és un exploit per tal d'executar comandes de forma remota dins del ordinador mitjançant el samba:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/samba4.png "samba4")

Si ens tornem en la página de Rapid7 i baixem una mica, veurem totes les comandes que tindrme que executar dins del metasploit:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/samba5.png "samba5")

Peró amb 4 comandes podrem obtenir accés al exploit:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/samba6.png "samba6")

Un cop conectats, podrem veure amb quin usuari estem:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/samba7.png "samba7")

Com podrem veure som el usuari **root**, per tant podem fer el que vulguem.

### CVE

**CVE-2007-2447:** 
https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-2447 

UnrealIRCd
======

Si busquem el exploit de UnrealIRCd, podrem veure que només hi ha un exploit disponible:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/irc1.png "irc1")

Utilitzarem aquest exploit, ens mirarem les opcions i provarem el seu funcionament:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/irc2.png "irc2")

Executarem el **Run**, i veurem que entrem dins de la máquina amb l'usuari **Root**:

![alt text](https://gitlab.com/GiorgiMskhiladze/m16/raw/master/Metasploitable2/Imagenes/irc3.png "irc3")

### CVE

**CVE-2010-2075:** 
https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-2075

